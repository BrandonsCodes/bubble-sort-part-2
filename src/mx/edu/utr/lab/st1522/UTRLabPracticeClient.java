/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.edu.utr.lab.st1522;

import java.util.Random;
import mx.edu.lab.st1522.Sorting;

/**
 *
 * @author alumno
 */
public class UTRLabPracticeClient {

    private static final Random randomGenerator = new Random();
    private static int length;
    private static int array[];
    private static int indx;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        length = 10;
        array = new int[length];
        for (indx = 0; indx < length; indx++) {
            array[indx] = randomGenerator.nextInt(length);
            //System.out.println(array[indx]);
        }

        //int[] array = {5, 4, 3, 2, 1};
        Sorting.bubbleSort(array);
        for (int i = 0; i < array.length; i++) {
            System.out.println(array[i]);
        }
    }
}
